/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.gmail.chk09140813.simple_music_player;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        public static final int RemoteViews_image_size=0x7f050000;
        public static final int activity_image_size=0x7f050001;
        public static final int widget_margin=0x7f050002;
    }
    public static final class drawable {
        public static final int appwidget_preview=0x7f020000;
        public static final int cover=0x7f020001;
        public static final int default_artwork=0x7f020002;
        public static final int example_appwidget_preview=0x7f020003;
        public static final int ic_action_cancel=0x7f020004;
        public static final int ic_action_next=0x7f020005;
        public static final int ic_action_pause=0x7f020006;
        public static final int ic_action_play=0x7f020007;
        public static final int ic_action_previous=0x7f020008;
        public static final int ic_action_repeat=0x7f020009;
        public static final int ic_action_repeat_off=0x7f02000a;
        public static final int ic_action_repeat_on=0x7f02000b;
        public static final int ic_action_repeat_one=0x7f02000c;
        public static final int ic_action_shuffle=0x7f02000d;
        public static final int ic_action_shuffle_off=0x7f02000e;
        public static final int ic_action_shuffle_on=0x7f02000f;
        public static final int ic_action_undo=0x7f020010;
        public static final int ic_drawer=0x7f020011;
        public static final int ic_launcher=0x7f020012;
        public static final int undo_bar_bg=0x7f020013;
        public static final int widget_bg=0x7f020014;
    }
    public static final class id {
        public static final int btnClose_n=0x7f090013;
        public static final int btnNext=0x7f09000c;
        public static final int btnNext_n=0x7f090012;
        public static final int btnNext_w=0x7f090019;
        public static final int btnPlay=0x7f09000b;
        public static final int btnPlay_n=0x7f090011;
        public static final int btnPlay_w=0x7f090018;
        public static final int btnPrev=0x7f09000a;
        public static final int btnPrev_n=0x7f090010;
        public static final int btnPrev_w=0x7f090017;
        public static final int btnRepeat=0x7f09001d;
        public static final int btnShuffle=0x7f09001e;
        public static final int coverArt=0x7f09001a;
        public static final int cover_art=0x7f090004;
        public static final int cover_art_n=0x7f09000d;
        public static final int cover_art_w=0x7f090014;
        public static final int currentPos=0x7f090007;
        public static final int duration=0x7f090008;
        public static final int media_controller=0x7f090009;
        public static final int media_controller_n=0x7f09000f;
        public static final int media_controller_w=0x7f090016;
        public static final int seekBar1=0x7f090006;
        public static final int songArtist=0x7f09001c;
        public static final int songTitle=0x7f09001b;
        public static final int song_list_view=0x7f090000;
        public static final int song_title=0x7f090005;
        public static final int song_title_n=0x7f09000e;
        public static final int song_title_w=0x7f090015;
        public static final int undo_bar=0x7f090001;
        public static final int undo_button=0x7f090003;
        public static final int undo_message=0x7f090002;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int notification=0x7f030001;
        public static final int simple_music_player_widget=0x7f030002;
        public static final int song_list_view=0x7f030003;
    }
    public static final class menu {
        public static final int menu=0x7f080000;
    }
    public static final class string {
        public static final int action_search=0x7f060000;
        public static final int action_settings=0x7f060001;
        public static final int add_widget=0x7f060002;
        public static final int app_eula=0x7f060003;
        public static final int app_name=0x7f060004;
        public static final int appwidget_text=0x7f060005;
        public static final int btnClose=0x7f060006;
        public static final int btnNext=0x7f060007;
        public static final int btnPlay=0x7f060008;
        public static final int btnPrev=0x7f060009;
        public static final int btnRepeat=0x7f06000a;
        public static final int btnShuffle=0x7f06000b;
        public static final int button_close_this_window=0x7f06000c;
        public static final int button_show_eula=0x7f06000d;
        public static final int cover_art=0x7f06000e;
        public static final int deleted=0x7f06000f;
        public static final int no_song=0x7f060010;
        public static final int please_agree_or_decline=0x7f060011;
        public static final int song_artist=0x7f060012;
        public static final int song_title=0x7f060013;
        public static final int undo=0x7f060014;
    }
    public static final class style {
        /** 
            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
    }
    public static final class xml {
        public static final int simple_music_player_widget_info=0x7f040000;
    }
}
