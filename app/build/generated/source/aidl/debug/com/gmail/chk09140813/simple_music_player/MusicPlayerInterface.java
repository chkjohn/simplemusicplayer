/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: E:\\AndroidStudioProjects\\SimpleMusicPlayer\\app\\src\\main\\aidl\\com\\gmail\\chk09140813\\simple_music_player\\MusicPlayerInterface.aidl
 */
package com.gmail.chk09140813.simple_music_player;
public interface MusicPlayerInterface extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.gmail.chk09140813.simple_music_player.MusicPlayerInterface
{
private static final java.lang.String DESCRIPTOR = "com.gmail.chk09140813.simple_music_player.MusicPlayerInterface";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.gmail.chk09140813.simple_music_player.MusicPlayerInterface interface,
 * generating a proxy if needed.
 */
public static com.gmail.chk09140813.simple_music_player.MusicPlayerInterface asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.gmail.chk09140813.simple_music_player.MusicPlayerInterface))) {
return ((com.gmail.chk09140813.simple_music_player.MusicPlayerInterface)iin);
}
return new com.gmail.chk09140813.simple_music_player.MusicPlayerInterface.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_basicTypes:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
long _arg1;
_arg1 = data.readLong();
boolean _arg2;
_arg2 = (0!=data.readInt());
float _arg3;
_arg3 = data.readFloat();
double _arg4;
_arg4 = data.readDouble();
java.lang.String _arg5;
_arg5 = data.readString();
this.basicTypes(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
reply.writeNoException();
return true;
}
case TRANSACTION_getPid:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getPid();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_setMessenger:
{
data.enforceInterface(DESCRIPTOR);
android.os.Messenger _arg0;
if ((0!=data.readInt())) {
_arg0 = android.os.Messenger.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.setMessenger(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_getDuration:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getDuration();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_getCurrentPosition:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getCurrentPosition();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_seekTo:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.seekTo(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_start:
{
data.enforceInterface(DESCRIPTOR);
this.start();
reply.writeNoException();
return true;
}
case TRANSACTION_pause:
{
data.enforceInterface(DESCRIPTOR);
this.pause();
reply.writeNoException();
return true;
}
case TRANSACTION_isPlaying:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isPlaying();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getRepeatState:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getRepeatState();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_isShuffle:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isShuffle();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getCurrentSong:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getCurrentSong();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_getLastSong:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getLastSong();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_setCurrentSong:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.setCurrentSong(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_deleteSong:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.deleteSong(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_undoDeletion:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
com.gmail.chk09140813.simple_music_player.Song _arg1;
if ((0!=data.readInt())) {
_arg1 = com.gmail.chk09140813.simple_music_player.Song.CREATOR.createFromParcel(data);
}
else {
_arg1 = null;
}
this.undoDeletion(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_Play:
{
data.enforceInterface(DESCRIPTOR);
this.Play();
reply.writeNoException();
return true;
}
case TRANSACTION_Prev:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.Prev(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_Next:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.Next(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_Shuffle:
{
data.enforceInterface(DESCRIPTOR);
this.Shuffle();
reply.writeNoException();
return true;
}
case TRANSACTION_Repeat:
{
data.enforceInterface(DESCRIPTOR);
this.Repeat();
reply.writeNoException();
return true;
}
case TRANSACTION_playSong:
{
data.enforceInterface(DESCRIPTOR);
com.gmail.chk09140813.simple_music_player.Song _arg0;
if ((0!=data.readInt())) {
_arg0 = com.gmail.chk09140813.simple_music_player.Song.CREATOR.createFromParcel(data);
}
else {
_arg0 = null;
}
this.playSong(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.gmail.chk09140813.simple_music_player.MusicPlayerInterface
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
/**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
@Override public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, java.lang.String aString) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(anInt);
_data.writeLong(aLong);
_data.writeInt(((aBoolean)?(1):(0)));
_data.writeFloat(aFloat);
_data.writeDouble(aDouble);
_data.writeString(aString);
mRemote.transact(Stub.TRANSACTION_basicTypes, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public int getPid() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getPid, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Pass a Messenger from client to service so that the service can send
     * feedback to the client
     * @param replyTo Messenger from client
     */
@Override public void setMessenger(android.os.Messenger replyTo) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((replyTo!=null)) {
_data.writeInt(1);
replyTo.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_setMessenger, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * Return the duration of current song
     * @return the duration in milliseconds, if no duration is available
     * (for example, if streaming live content), -1 is returned.
     */
@Override public int getDuration() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getDuration, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Return the current position of music player
     * @return the current position in milliseconds
     */
@Override public int getCurrentPosition() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getCurrentPosition, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Seek the music player to specific time position
     * @param progress the offset in milliseconds from the start to seek to
     */
@Override public void seekTo(int progress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(progress);
mRemote.transact(Stub.TRANSACTION_seekTo, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * start the music player
     */
@Override public void start() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_start, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * pause the music player
     */
@Override public void pause() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_pause, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * check if the music player is playing
     * @return true if playing, false otherwise
     */
@Override public boolean isPlaying() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isPlaying, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * Return the current state of 'repeat' function (OFF, ON, ONE)
     * @return An integer representing the current state of 'repeat' function
     */
@Override public int getRepeatState() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getRepeatState, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * check if the 'shuffle' function is ON
     * @return true for ON, false for OFF
     */
@Override public boolean isShuffle() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isShuffle, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * get the position of current song
     * @return position of the song in the song list
     */
@Override public int getCurrentSong() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getCurrentSong, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * get the position of last song
     * @return position of the song in the song list
     */
@Override public int getLastSong() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getLastSong, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
/**
     * select the song at position 'current' as current song
     * @param current position of the song
     */
@Override public void setCurrentSong(int current) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(current);
mRemote.transact(Stub.TRANSACTION_setCurrentSong, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * delete the song at the index 'position' of song list
     * @param position position of the song to be deleted
     */
@Override public void deleteSong(int position) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(position);
mRemote.transact(Stub.TRANSACTION_deleteSong, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * undo the previous deletion
     * @param position original position of the deleted song in
     *                 the song list
     * @param song the deleted song
     */
@Override public void undoDeletion(int position, com.gmail.chk09140813.simple_music_player.Song tmp) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(position);
if ((tmp!=null)) {
_data.writeInt(1);
tmp.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_undoDeletion, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * toggle the 'play' function
     */
@Override public void Play() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_Play, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * select and play the previous song
     * @param currentSong position of current song
     */
@Override public void Prev(int current) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(current);
mRemote.transact(Stub.TRANSACTION_Prev, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * select and play the next song
     * @param currentSong position of current song
     */
@Override public void Next(int current) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(current);
mRemote.transact(Stub.TRANSACTION_Next, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * toogle the 'shuffle' function
     */
@Override public void Shuffle() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_Shuffle, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * toggle the 'repeat' function
     */
@Override public void Repeat() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_Repeat, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
/**
     * reset, prepare and start the media player
     * @param song the song that will be played
     */
@Override public void playSong(com.gmail.chk09140813.simple_music_player.Song song) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
if ((song!=null)) {
_data.writeInt(1);
song.writeToParcel(_data, 0);
}
else {
_data.writeInt(0);
}
mRemote.transact(Stub.TRANSACTION_playSong, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_basicTypes = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_getPid = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_setMessenger = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_getDuration = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_getCurrentPosition = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_seekTo = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_start = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_pause = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_isPlaying = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_getRepeatState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_isShuffle = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
static final int TRANSACTION_getCurrentSong = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
static final int TRANSACTION_getLastSong = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
static final int TRANSACTION_setCurrentSong = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
static final int TRANSACTION_deleteSong = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
static final int TRANSACTION_undoDeletion = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
static final int TRANSACTION_Play = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
static final int TRANSACTION_Prev = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
static final int TRANSACTION_Next = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
static final int TRANSACTION_Shuffle = (android.os.IBinder.FIRST_CALL_TRANSACTION + 19);
static final int TRANSACTION_Repeat = (android.os.IBinder.FIRST_CALL_TRANSACTION + 20);
static final int TRANSACTION_playSong = (android.os.IBinder.FIRST_CALL_TRANSACTION + 21);
}
/**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, java.lang.String aString) throws android.os.RemoteException;
public int getPid() throws android.os.RemoteException;
/**
     * Pass a Messenger from client to service so that the service can send
     * feedback to the client
     * @param replyTo Messenger from client
     */
public void setMessenger(android.os.Messenger replyTo) throws android.os.RemoteException;
/**
     * Return the duration of current song
     * @return the duration in milliseconds, if no duration is available
     * (for example, if streaming live content), -1 is returned.
     */
public int getDuration() throws android.os.RemoteException;
/**
     * Return the current position of music player
     * @return the current position in milliseconds
     */
public int getCurrentPosition() throws android.os.RemoteException;
/**
     * Seek the music player to specific time position
     * @param progress the offset in milliseconds from the start to seek to
     */
public void seekTo(int progress) throws android.os.RemoteException;
/**
     * start the music player
     */
public void start() throws android.os.RemoteException;
/**
     * pause the music player
     */
public void pause() throws android.os.RemoteException;
/**
     * check if the music player is playing
     * @return true if playing, false otherwise
     */
public boolean isPlaying() throws android.os.RemoteException;
/**
     * Return the current state of 'repeat' function (OFF, ON, ONE)
     * @return An integer representing the current state of 'repeat' function
     */
public int getRepeatState() throws android.os.RemoteException;
/**
     * check if the 'shuffle' function is ON
     * @return true for ON, false for OFF
     */
public boolean isShuffle() throws android.os.RemoteException;
/**
     * get the position of current song
     * @return position of the song in the song list
     */
public int getCurrentSong() throws android.os.RemoteException;
/**
     * get the position of last song
     * @return position of the song in the song list
     */
public int getLastSong() throws android.os.RemoteException;
/**
     * select the song at position 'current' as current song
     * @param current position of the song
     */
public void setCurrentSong(int current) throws android.os.RemoteException;
/**
     * delete the song at the index 'position' of song list
     * @param position position of the song to be deleted
     */
public void deleteSong(int position) throws android.os.RemoteException;
/**
     * undo the previous deletion
     * @param position original position of the deleted song in
     *                 the song list
     * @param song the deleted song
     */
public void undoDeletion(int position, com.gmail.chk09140813.simple_music_player.Song tmp) throws android.os.RemoteException;
/**
     * toggle the 'play' function
     */
public void Play() throws android.os.RemoteException;
/**
     * select and play the previous song
     * @param currentSong position of current song
     */
public void Prev(int current) throws android.os.RemoteException;
/**
     * select and play the next song
     * @param currentSong position of current song
     */
public void Next(int current) throws android.os.RemoteException;
/**
     * toogle the 'shuffle' function
     */
public void Shuffle() throws android.os.RemoteException;
/**
     * toggle the 'repeat' function
     */
public void Repeat() throws android.os.RemoteException;
/**
     * reset, prepare and start the media player
     * @param song the song that will be played
     */
public void playSong(com.gmail.chk09140813.simple_music_player.Song song) throws android.os.RemoteException;
}
