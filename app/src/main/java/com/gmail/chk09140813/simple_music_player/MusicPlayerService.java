package com.gmail.chk09140813.simple_music_player;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.RemoteControlClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.PowerManager;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;

import com.gmail.chk09140813.simple_music_player.MainActivity.repeatState;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;
import static android.media.AudioManager.AUDIOFOCUS_LOSS;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;
import static android.media.AudioManager.STREAM_MUSIC;
import static com.gmail.chk09140813.simple_music_player.Database.CURRENT_SONG_TABLE;
import static com.gmail.chk09140813.simple_music_player.Database.NOW_PLAYING_TABLE;
import static com.gmail.chk09140813.simple_music_player.Database.REPEAT_SHUFFLE_TABLE;

/**
 * Project: SimpleMusicPlayer
 * Arthur:  Chan Ho Kwan (John)
 */

public class MusicPlayerService extends Service implements
        OnCompletionListener, OnAudioFocusChangeListener {

    public static final String ACTION_PREV
            = "com.gmail.chk09140813.simple_music_player.ACTION_PREV";
    public static final String ACTION_PLAY
            = "com.gmail.chk09140813.simple_music_player.ACTION_PLAY";
    public static final String ACTION_NEXT
            = "com.gmail.chk09140813.simple_music_player.ACTION_NEXT";
    public static final String ACTION_CANCEL
            = "com.gmail.chk09140813.simple_music_player.ACTION_CANCEL";
    public static final String ACTION_UPDATE_WIDGET
            = "com.gmail.chk09140813.simple_music_player.ACTION_UPDATE_WIDGET";
    public static final String BOOT_COMPLETED
            = "com.gmail.chk09140813.simple_music_player.BOOT_COMPLETED";

    // Keys to identify media player actions
    public static final int PREV = 0;
	public static final int PLAY = 1;
	public static final int NEXT = 2;
	public static final int CANCEL = 5;
    public static final int COMPLETE = 6;

    private static final int ID = 9998;
    private PendingIntent pendingIntent;
    private RemoteViews notificationView;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder musicPlayerNotification;

    private AppWidgetManager appWidgetManager;
    private int[] allWidgetIds;
	
	private repeatState isRepeat = repeatState.OFF;
	private boolean isShuffle = false;
	private boolean isPlaying = false;
	
	private SongListAdapter songListAdapter;

	private Random rand;
    private Song song;                      // current playing song
    private Integer currentSong;			// position of current playing song
    private Integer lastSong;
	private ArrayList<Integer> songPos;		// list of positions of all songs played
    private ArrayList<Song> nowPlayingList; // list of all songs player
	private MediaPlayer mediaPlayer;

    private Bitmap playIcon;
    private Bitmap pauseIcon;

    private Handler mHandler;
    private Runnable mRunnable;

    protected Messenger mMessenger;       // used to communicate with MainActivity
    private final LocalInterface mBinder = new LocalInterface();  // Binder given to clients
    private AudioManager audioManager;
    private RemoteControlClient mRemoteControlClient;
    private PendingIntent mediaPendingIntent;

    private ComponentName mReceiverComponent;
    private ComponentName widget;
    private PhoneStateListener phoneStateListener;
    private TelephonyManager telephonyManager;

    @Override
    public void onAudioFocusChange(int i) {
        if (mediaPlayer != null) {
            switch (i) {
                case AUDIOFOCUS_GAIN:
                    if (isPlaying && !mediaPlayer.isPlaying())
                        mediaPlayer.start();
                    else if (mediaPlayer.isPlaying())
                        mediaPlayer.setVolume(1.0f, 1.0f);
                    break;
                case AUDIOFOCUS_LOSS:
                case AUDIOFOCUS_LOSS_TRANSIENT:
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.pause();
                        updateMusicPlayerWidget(null);
                        setNotificationView();
                        callMusicPlayerNotification();
                        setRemoteControlView();
                    }
                case AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    if (mediaPlayer.isPlaying())
                        mediaPlayer.setVolume(0.1f, 0.1f);
                    break;
            }
        }
    }

    public class LocalInterface extends MusicPlayerInterface.Stub{

        @Override
        public void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat, double aDouble, String aString) throws RemoteException {

        }

        @Override
        public int getPid() throws RemoteException {
            return android.os.Process.myPid();
        }

        @Override
        public void setMessenger(Messenger replyTo) throws RemoteException {
            mMessenger = replyTo;
        }

        @Override
        public int getDuration() throws RemoteException {
            return mediaPlayer.getDuration();
        }

        @Override
        public int getCurrentPosition() throws RemoteException {
            return mediaPlayer.getCurrentPosition();
        }

        @Override
        public void seekTo(int progress) throws RemoteException {
            mediaPlayer.seekTo(progress);
        }

        @Override
        public void start() throws RemoteException {
            mediaPlayer.start();
        }

        @Override
        public void pause() throws RemoteException {
            mediaPlayer.pause();
        }

        @Override
        public boolean isPlaying() throws RemoteException {
            return isPlaying;
        }

        @Override
        public int getRepeatState() throws RemoteException {
            return isRepeat.ordinal();
        }

        @Override
        public boolean isShuffle() throws RemoteException {
            return isShuffle;
        }

        @Override
        public int getCurrentSong() throws RemoteException {
            return currentSong;
        }

        @Override
        public int getLastSong() throws RemoteException {
            return lastSong;
        }

        @Override
        public void setCurrentSong(int current) throws RemoteException {
            lastSong = currentSong;
            currentSong = current;
            song = (Song) songListAdapter.getItem(current);
            if (!songPos.contains(current) && !lastSong.equals(currentSong)) {
                int lastSongPos = songPos.lastIndexOf(lastSong);
                if (lastSongPos + 1 != songPos.size()) {
                    songPos.add(lastSongPos + 1, current);
                    nowPlayingList.add(lastSongPos + 1, song);
                } else {
                    songPos.add(current);
                    nowPlayingList.add(song);
                }
            }
        }

        @Override
        public void deleteSong(int position) throws RemoteException {
            Song toBeDelete = (Song) songListAdapter.getItem(position);
            if (nowPlayingList.contains(toBeDelete)) {
                songPos.remove(nowPlayingList.indexOf(toBeDelete));
                nowPlayingList.remove(toBeDelete);
            }
            songListAdapter.remove(position);
            if (songListAdapter.getCount() > 0) {
                if (currentSong == position) {
                    Next(lastSong);
                    if (mMessenger != null)
                        sendMessage(NEXT);
                } else if (position < currentSong)
                    currentSong--;
                if (position < lastSong)
                    lastSong--;

                for (int i = 0; i < songPos.size(); i++) {
                    int pos = songPos.get(i);
                    if (position <= pos)
                        songPos.set(i, pos - 1);
                }
            } else if (currentSong == position){
                mediaPlayer.stop();
                song = null;
                isPlaying = false;
                mRemoteControlClient.setPlaybackState(
                        RemoteControlClient.PLAYSTATE_STOPPED);
            }
        }

        @Override
        public void undoDeletion(int position, Song tmp) throws RemoteException {
            songListAdapter.add(position, tmp);
            if (position <= currentSong)
                currentSong++;
            if (position <= lastSong)
                lastSong++;
            for (int i = 0; i < songPos.size(); i++){
                int pos = songPos.get(i);
                if (position <= pos)
                    songPos.set(i, pos + 1);
            }
            if (song == null){
                song = (Song) songListAdapter.getItem(currentSong);
                nowPlayingList.add(song);
                songPos.add(currentSong);
                try{
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(getApplicationContext(),
                            Uri.parse(song.getPath()));
                    mediaPlayer.prepare();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void Play() throws RemoteException {
            _Play();
        }

        @Override
        public void Prev(int current) throws RemoteException {
            _Prev(current);
        }

        @Override
        public void Next(int current) throws RemoteException {
            _Next(current);
        }

        @Override
        public void Shuffle() throws RemoteException {
            _Shuffle();
        }

        @Override
        public void Repeat() throws RemoteException {
            _Repeat();
        }

        @Override
        public void playSong(Song song) throws RemoteException {
            _playSong(song);
        }
    }

    /**
     *  toggle the media player
     */
    private void _Play(){
        if (mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            isPlaying = false;
            mHandler.postDelayed(mRunnable, 30*60*1000);
        } else{
            mediaPlayer.start();
            isPlaying = true;
            mHandler.removeCallbacks(mRunnable);
        }
    }

    /**
     * set media player to play the previous song
     * @param currentSong position of current playing song
     */
    private void _Prev(Integer currentSong){
        lastSong = currentSong;
        int prevSong = songPos.lastIndexOf(currentSong) - 1;
        if (prevSong < 0){
            if (songPos.size() == songListAdapter.getCount()){
                this.currentSong = songPos.get(songPos.size() - 1);
            } else if (isShuffle){
                do{
                    this.currentSong = rand.nextInt(songListAdapter.getCount());
                } while(songPos.contains(this.currentSong));
                songPos.add(0, this.currentSong);
            } else {
                this.currentSong = (currentSong > 0) ?
                        currentSong - 1 : songListAdapter.getCount() - 1;
                songPos.add(0, this.currentSong);
            }
        } else {
            this.currentSong = songPos.get(prevSong);
        }
        song = (Song) songListAdapter.getItem(this.currentSong);
        if (!nowPlayingList.contains(song)){
            nowPlayingList.add(0, song);
        }
        _playSong(song);
        mHandler.removeCallbacks(mRunnable);
    }

    /**
     * set media player to play the next song
     * @param currentSong position of current playing song
     */
    private void _Next(Integer currentSong){
        lastSong = currentSong;
        int nextSong = songPos.lastIndexOf(currentSong) + 1;
        if (nextSong >= songPos.size()){
            if (songPos.size() == songListAdapter.getCount()){
                this.currentSong = songPos.get(0);
            } else if (isShuffle){
                do{
                    this.currentSong = rand.nextInt(songListAdapter.getCount());
                } while(songPos.contains(this.currentSong));
                songPos.add(this.currentSong);
            } else {
                this.currentSong = (currentSong + 1) % songListAdapter.getCount();
                songPos.add(this.currentSong);
            }
        } else{
            this.currentSong = songPos.get(nextSong);
        }
        song = (Song) songListAdapter.getItem(this.currentSong);
        if (!nowPlayingList.contains(song)){
            nowPlayingList.add(song);
        }
        _playSong(song);
        mHandler.removeCallbacks(mRunnable);
    }

    /**
     * toggle the 'Shuffle' function
     */
    private void _Shuffle(){
        isShuffle = !isShuffle;
        songPos.clear();
        songPos.add(currentSong);
        nowPlayingList.clear();
        nowPlayingList.add(song);
        mHandler.removeCallbacks(mRunnable);
        mHandler.postDelayed(mRunnable, 30*60*1000);
    }

    /**
     * toggle the 'Repeat' function
     */
    private void _Repeat(){
        switch(isRepeat.ordinal()){
            case 0:	// repeat OFF
                mediaPlayer.setLooping(false);
                isRepeat = repeatState.ON;
                break;
            case 1:	// repeat ON
                mediaPlayer.setLooping(true);
                isRepeat = repeatState.ONE;
                break;
            case 2:	// repeat one song only
                mediaPlayer.setLooping(false);
                isRepeat = repeatState.OFF;
                break;
        }
        mHandler.removeCallbacks(mRunnable);
        mHandler.postDelayed(mRunnable, 30*60*1000);
    }

    /**
     * reset the media player
     * @param song current playing song
     */
    private void _playSong(Song song){
        try {
            mediaPlayer.reset();
            mediaPlayer.setDataSource(this, Uri.parse(song.getPath()));
            mediaPlayer.prepare();

            mediaPlayer.start();
            isPlaying = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

    @Override
    public boolean onUnbind(Intent intent) {
        mMessenger = null;
        if (!isPlaying)
            stopSelf();
        return true;
    }

    /**
     * initialize the media player
     */
    public void initMediaPlayer(){
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setAudioStreamType(STREAM_MUSIC);
        mediaPlayer.setWakeMode(this, PowerManager.PARTIAL_WAKE_LOCK);
        try{
            mediaPlayer.setDataSource(this, Uri.parse(song.getPath()));
            mediaPlayer.prepare();
        } catch (Exception e){
            //e.printStackTrace();
        }
    }

    @Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();

        rand = new Random(Calendar.getInstance().getTimeInMillis());

        /**
         * get data of playlist from database
         */
        songListAdapter = new SongListAdapter(this);
        Database.init(this);
        song = Database.getCurrentSong();
        nowPlayingList = Database.getNowPlayingList();
        Database.deleteAll(CURRENT_SONG_TABLE);
        Database.deleteAll(NOW_PLAYING_TABLE);
        if (songListAdapter.getCount() > 0) {
            if (song == null) {
                if (nowPlayingList.size() > 0) {
                    song = nowPlayingList.get(nowPlayingList.size() - 1);
                    currentSong = songListAdapter.indexOf(song);
                } else {
                    currentSong = rand.nextInt(songListAdapter.getCount());
                    song = (Song) songListAdapter.getItem(currentSong);
                    nowPlayingList.add(song);
                }
            }
            currentSong = songListAdapter.indexOf(song);
            if (nowPlayingList.size() > 1 && nowPlayingList.indexOf(song) >= 1) {
                Song last = nowPlayingList.get(nowPlayingList.indexOf(song) - 1);
                lastSong = songListAdapter.indexOf(last);
            } else {
                lastSong = -1;
            }
        } else{
            currentSong = -1;
            lastSong = -1;
        }

        initMediaPlayer();
        songPos = new ArrayList<>();
        initSongPos();

        /**
         *  get states of 'repeat' and 'shuffle' functions from database
         */
        int repeat = Database.isRepeat();
        switch (repeat){
            case 0:	// repeat OFF
                mediaPlayer.setLooping(false);
                isRepeat = repeatState.OFF;
                break;
            case 1:	// repeat ON
                mediaPlayer.setLooping(false);
                isRepeat = repeatState.ON;
                break;
            case 2:	// repeat one song only
                mediaPlayer.setLooping(true);
                isRepeat = repeatState.ONE;
                break;
        }
        int shuffle = Database.isShuffle();
        isShuffle = (shuffle > 0);

        Database.deleteAll(REPEAT_SHUFFLE_TABLE);

        /**
         *  notification config
         */
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        pendingIntent = PendingIntent.getActivity(
                this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        /**
         *  widget config
         */
        widget = new ComponentName(this, SimpleMusicPlayerWidget.class);
        appWidgetManager = AppWidgetManager.getInstance(this);
        allWidgetIds = appWidgetManager.getAppWidgetIds(widget);

        /**
         *  set up idle handler
         */
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                stopSelf();
            }
        };

        /**
         *  set up Remote Control Client
         */
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mReceiverComponent = new ComponentName(this,
                RemoteControlBroadcastReceiver.class.getName());
        audioManager.registerMediaButtonEventReceiver(mReceiverComponent);
        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        mediaButtonIntent.setComponent(mReceiverComponent);
        mediaPendingIntent=PendingIntent.getBroadcast(this,
                0, mediaButtonIntent, 0);

        /**
         *  set up phone state listener
         */
        phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                switch (state) {
                    case TelephonyManager.CALL_STATE_RINGING:
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        //Incoming call: Pause music
                        if (mediaPlayer.isPlaying())
                            mediaPlayer.pause();
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        //Not in call: Play music
                        if (isPlaying && !mediaPlayer.isPlaying())
                            mediaPlayer.start();
                        break;
                }

                super.onCallStateChanged(state, incomingNumber);
            }
        };
        telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        if(telephonyManager != null) {
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
        //Log.d(this.getClass().getSimpleName(), "onStartCommand()");

        String action = null;
        if (intent != null)
            action = intent.getAction();
        if (action != null) {
            Bundle data = intent.getExtras();
            switch (action) {
                case ACTION_CANCEL:
                    if (mMessenger != null)
                        sendMessage(CANCEL);
                    stopSelf();
                    return super.onStartCommand(intent, flags, startId);
                case ACTION_PREV:
                    if (songListAdapter.getCount() > 0) {
                        _Prev(currentSong);
                        if (mMessenger != null)
                            sendMessage(PREV);
                    }
                    break;
                case ACTION_PLAY:
                    if (songListAdapter.getCount() > 0) {
                        _Play();
                        if (mMessenger != null)
                            sendMessage(PLAY);
                    }
                    break;
                case ACTION_NEXT:
                    if (songListAdapter.getCount() > 0) {
                        _Next(currentSong);
                        if (mMessenger != null)
                            sendMessage(NEXT);
                    }
                    break;
                case BOOT_COMPLETED:
                case ACTION_UPDATE_WIDGET:
                    updateMusicPlayerWidget(data);
                    stopSelf();
                    return super.onStartCommand(intent, flags, startId);
            }
        }
        updateMusicPlayerWidget(null);
        setNotificationView();
        callMusicPlayerNotification();
        setRemoteControlView();

		return super.onStartCommand(intent, flags, startId);
	}

    @Override
    public void onDestroy() {
        super.onDestroy();
        mediaPlayer.release();
        mediaPlayer = null;
        isPlaying = false;
        updateMusicPlayerWidget(null);
        audioManager.unregisterMediaButtonEventReceiver(mReceiverComponent);
        audioManager.unregisterRemoteControlClient(mRemoteControlClient);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        if (song != null) {
            Database.saveCurrentSong(song);
            song = null;
        }
        Database.saveRepeatShuffle(isRepeat.ordinal(), (isShuffle) ? 1 : 0);
        saveNowPlayingList();
        Database.deactivate();
        mHandler.removeCallbacks(mRunnable);
        stopForeground(true);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (level >= TRIM_MEMORY_RUNNING_LOW)
            System.gc();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        // TODO Auto-generated method stub
        if (isRepeat == repeatState.ON){
            if (mMessenger != null) {
                sendMessage(NEXT);
            } else{
                _Next(currentSong);
                setNotificationView();
                callMusicPlayerNotification();
                updateMusicPlayerWidget(null);
                setRemoteControlView();
            }
        } else if (isRepeat == repeatState.ONE && !mediaPlayer.isLooping()) {
            mediaPlayer.setLooping(true);
        } else {
            isPlaying = false;
            if (mMessenger != null)
                sendMessage(COMPLETE);
            setNotificationView();
            callMusicPlayerNotification();
            updateMusicPlayerWidget(null);
            setRemoteControlView();
        }
    }

    private void saveNowPlayingList(){
        for (Song song: nowPlayingList){
            Database.saveNowPlayingSong(song);
        }
    }

    private void setRemoteControlView(){
        mRemoteControlClient = new RemoteControlClient(mediaPendingIntent);
        mRemoteControlClient.setTransportControlFlags(
                RemoteControlClient.FLAG_KEY_MEDIA_PLAY_PAUSE
                        | RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS
                        | RemoteControlClient.FLAG_KEY_MEDIA_NEXT);
        if (isPlaying){
            mRemoteControlClient.setPlaybackState(
                    RemoteControlClient.PLAYSTATE_PLAYING);
        } else{
            mRemoteControlClient.setPlaybackState(
                    RemoteControlClient.PLAYSTATE_PAUSED);
        }
        mRemoteControlClient.editMetadata(true)
                .putString(MediaMetadataRetriever.
                        METADATA_KEY_TITLE, song.getTitle())
                .apply();
        audioManager.registerRemoteControlClient(mRemoteControlClient);
        audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
    }

    private void initSongPos(){
        for (Song song : nowPlayingList) {
            int pos = songListAdapter.indexOf(song);
            songPos.add(pos);
        }
    }

    private void setNotificationView(){
        if (notificationView == null) {
            notificationView = new RemoteViews(getPackageName(),
                    R.layout.notification);
            setOnButtonClickListener(notificationView, false);
        }
        setViewPlayButton(notificationView, R.id.btnPlay_n);
        if (song != null) {
            notificationView.setTextViewText(R.id.song_title_n, song.getTitle());
        } else{
            notificationView.setTextViewText(R.id.song_title_n, getString(R.string.no_song));
        }
        notificationView.setImageViewResource(R.id.cover_art_n, R.drawable.cover);
    }

    private void setWidgetView(RemoteViews view){
        setViewPlayButton(view, R.id.btnPlay_w);
        if (song != null) {
            view.setTextViewText(R.id.song_title_w, song.getTitle());
        } else{
            view.setTextViewText(R.id.song_title_w, getString(R.string.no_song));
        }
        view.setImageViewResource(R.id.cover_art_w, R.drawable.cover);
    }

    private void setViewPlayButton(RemoteViews view, int resId){
        if (isPlaying){
            if (pauseIcon == null)
                pauseIcon = BitmapFactory.decodeResource(
                        getResources(), R.drawable.ic_action_pause);
            view.setImageViewBitmap(resId, pauseIcon);
        } else{
            if (playIcon == null)
                playIcon = BitmapFactory.decodeResource(
                        getResources(), R.drawable.ic_action_play);
            view.setImageViewBitmap(resId, playIcon);
        }
    }

    private void callMusicPlayerNotification(){
        if (musicPlayerNotification == null) {
            musicPlayerNotification = new NotificationCompat.Builder(this).
                    setSmallIcon(R.drawable.ic_launcher).
                    setContentIntent(pendingIntent).
                    setOngoing(true).
                    setAutoCancel(false).
                    setPriority(Notification.PRIORITY_MAX).
                    setTicker(getString(R.string.app_name), notificationView);
        }
        musicPlayerNotification.setContent(notificationView);

        Notification no = musicPlayerNotification.build();
        no.flags |= Notification.FLAG_NO_CLEAR;

        Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, song.getAlbumID());
        Picasso.with(this)
                .load(uri)
                .resizeDimen(R.dimen.RemoteViews_image_size, R.dimen.RemoteViews_image_size)
                //.centerInside()
                .centerCrop()
                .tag(this)
                .into(notificationView, R.id.cover_art_n, ID, no);

        if (notificationManager == null) {
            notificationManager = (NotificationManager)
                    getSystemService(Context.NOTIFICATION_SERVICE);
            startForeground(ID, no);
        } else{
            notificationManager.notify(ID, no);
        }
    }

    private void updateMusicPlayerWidget(Bundle data){
        if (data != null) {
            allWidgetIds = data.getIntArray(
                    AppWidgetManager.EXTRA_APPWIDGET_IDS);
        }

        if (allWidgetIds == null) {
            allWidgetIds = appWidgetManager.getAppWidgetIds(widget);
        }

        RemoteViews view = new RemoteViews(getPackageName(),
                R.layout.simple_music_player_widget);
        setOnButtonClickListener(view, true);
        setWidgetView(view);

        Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, song.getAlbumID());
        Picasso.with(this)
                .load(uri)
                .resizeDimen(R.dimen.RemoteViews_image_size, R.dimen.RemoteViews_image_size)
                //.centerInside()
                .centerCrop()
                .tag(this)
                .into(view, R.id.cover_art_w, allWidgetIds);

        for (int allWidgetId : allWidgetIds) {
            appWidgetManager.updateAppWidget(allWidgetId, view);
        }
    }

    private void setOnButtonClickListener(RemoteViews view, boolean isWidget){
        Intent prev = new Intent(ACTION_PREV);
        PendingIntent pPrev = PendingIntent.getService(
                this, 0, prev, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent play = new Intent(ACTION_PLAY);
        PendingIntent pPlay = PendingIntent.getService(
                this, 0, play, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent next = new Intent(ACTION_NEXT);
        PendingIntent pNext = PendingIntent.getService(
                this, 0, next, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent cancel = new Intent(ACTION_CANCEL);
        PendingIntent pCancel = PendingIntent.getService(
                this, 0, cancel, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent activity = new Intent(this, MainActivity.class);
        PendingIntent pActivity = PendingIntent.getActivity(
                this, 0, activity, PendingIntent.FLAG_UPDATE_CURRENT);
        if (isWidget){
            view.setOnClickPendingIntent(R.id.btnPrev_w, pPrev);
            view.setOnClickPendingIntent(R.id.btnPlay_w, pPlay);
            view.setOnClickPendingIntent(R.id.btnNext_w, pNext);
            view.setOnClickPendingIntent(R.id.cover_art_w, pActivity);
        } else {
            view.setOnClickPendingIntent(R.id.btnPrev_n, pPrev);
            view.setOnClickPendingIntent(R.id.btnPlay_n, pPlay);
            view.setOnClickPendingIntent(R.id.btnNext_n, pNext);
            view.setOnClickPendingIntent(R.id.btnClose_n, pCancel);
        }
    }

    private void sendMessage(int action){
        Message msg = Message.obtain(null, action);
        try {
            mMessenger.send(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
