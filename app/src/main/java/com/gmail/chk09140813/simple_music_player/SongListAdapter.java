package com.gmail.chk09140813.simple_music_player;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Project: SimpleMusicPlayer
 * Arthur:  Chan Ho Kwan (John)
 */

public class SongListAdapter implements ListAdapter{
	//song list and layout
	private Context context = null;
	private ArrayList<Song> songList = null;

    private static class ViewHolder{
        ImageView coverArt;
        TextView titleView;
        TextView artistView;
    }

	//constructor
	public SongListAdapter(Context context){
		setContext(context);
		if (songList == null) {
            songList = new ArrayList<>();

            ContentResolver musicResolver = context.getContentResolver();
            Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor musicCursor = musicResolver.query(musicUri, null, null, null,
                    MediaStore.Audio.Media.TITLE_KEY + " ASC");

            //iterate over results if valid
            if (musicCursor != null && musicCursor.moveToFirst()) {
                //get columns
                int titleColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                int idColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media._ID);
                int artistColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
                int albumColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
                int albumIdColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
                int dataColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
                int isMusicColumn = musicCursor.getColumnIndex(MediaStore.Audio.Media.IS_MUSIC);
                //add songs to list
                do {
                    int isMusic = Integer.parseInt(musicCursor.getString(isMusicColumn));
                    if (isMusic != 0) {
                        long thisId = musicCursor.getLong(idColumn);
                        long thisAlbumId = musicCursor.getLong(albumIdColumn);
                        String thisTitle = musicCursor.getString(titleColumn);
                        String thisAlbum = musicCursor.getString(albumColumn);
                        String thisArtist = musicCursor.getString(artistColumn);
                        String thisPath = musicCursor.getString(dataColumn);
                        songList.add(new Song(thisId, thisAlbumId, thisTitle,
                                thisAlbum, thisArtist, thisPath));
                    }
                } while (musicCursor.moveToNext());
                musicCursor.close();
            }
        }
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return songList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return songList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return true;
	}
	
	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
        return songList.size() == 0;
    }

	@Override
	public boolean areAllItemsEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return true;
	}

    public int indexOf(Song song){
        return songList.indexOf(song);
    }
	
	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        ViewHolder viewHolder;

        //map to song layout
        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.song_list_view, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.coverArt = (ImageView)convertView.findViewById(R.id.coverArt);
            viewHolder.titleView = (TextView)convertView.findViewById(R.id.songTitle);
            viewHolder.artistView = (TextView)convertView.findViewById(R.id.songArtist);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //get song using position
        Song currentSong = songList.get(position);
        //get title and artist strings
        viewHolder.titleView.setText(currentSong.getTitle());
        viewHolder.artistView.setText(currentSong.getArtist());
		// get the cover art image
        Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
        Uri uri = ContentUris.withAppendedId(sArtworkUri, currentSong.getAlbumID());
        Picasso.with(context)
                .load(uri)
                .placeholder(R.drawable.cover)
                .error(R.drawable.cover)
                .resizeDimen(R.dimen.RemoteViews_image_size, R.dimen.RemoteViews_image_size)
                //.centerInside()
                .centerCrop()
                .tag(context)
                .into(viewHolder.coverArt);

		return convertView;
	}

    public void remove(int position) {
        songList.remove(position);
    }

    public void add(int position, Song song){
        songList.add(position, song);
    }

}
