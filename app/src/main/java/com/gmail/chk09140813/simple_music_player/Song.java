package com.gmail.chk09140813.simple_music_player;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Project: SimpleMusicPlayer
 * Arthur:  Chan Ho Kwan (John)
 */

public class Song implements Parcelable {

	private long id;
	private long albumID;
	private String title;
	private String album;
	private String artist;
	private String path;
	
	public Song(long songID, long songAlbumID, String songTitle, String songAlbum,
                String songArtist, String songPath){
		id=songID;
		albumID=songAlbumID;
		title=songTitle;
		album=songAlbum;
		artist=songArtist;
		path=songPath;
	}
	
	public long getID(){return id;}
	public long getAlbumID(){return albumID;}
	public String getTitle(){return title;}
	public String getAlbum (){return album;}
	public String getArtist(){return artist;}
	public String getPath(){return path;}

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Song) {
            Song song = (Song) o;
            return this.getID() == song.getID();
        }
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeLong(id);
        out.writeLong(albumID);
        out.writeString(title);
        out.writeString(album);
        out.writeString(artist);
        out.writeString(path);
    }

    private Song(Parcel in){
        id = in.readLong();
        albumID = in.readLong();
        title = in.readString();
        album = in.readString();
        artist = in.readString();
        path = in.readString();
    }

    public static final Creator<Song> CREATOR = new Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int i) {
            return new Song[i];
        }
    };

}
