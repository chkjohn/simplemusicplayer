package com.gmail.chk09140813.simple_music_player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import static com.gmail.chk09140813.simple_music_player.MusicPlayerService.*;

/**
 * Project: SimpleMusicPlayer
 * Arthur:  Chan Ho Kwan (John)
 */

public class RemoteControlBroadcastReceiver extends BroadcastReceiver {
    public RemoteControlBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
        if (keyEvent.getAction() != KeyEvent.ACTION_DOWN)
            return;

        switch (keyEvent.getKeyCode()) {
            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                intent = new Intent(context, MusicPlayerService.class);
                intent.setAction(ACTION_PLAY);
                context.startService(intent);
                break;
            case KeyEvent.KEYCODE_MEDIA_NEXT:
                intent = new Intent(context, MusicPlayerService.class);
                intent.setAction(ACTION_NEXT);
                context.startService(intent);
                break;
            case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                intent = new Intent(context, MusicPlayerService.class);
                intent.setAction(ACTION_PREV);
                context.startService(intent);
                break;
        }
    }
}
