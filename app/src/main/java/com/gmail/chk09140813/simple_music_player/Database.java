package com.gmail.chk09140813.simple_music_player;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.io.File;
import java.util.ArrayList;

/**
 * Project: SimpleMusicPlayer
 * Arthur:  Chan Ho Kwan (John)
 */

public class Database extends SQLiteOpenHelper {
    static Context context = null;
	static Database instance = null;
	static SQLiteDatabase database = null;
	
	static final String DATABASE_NAME = "DB";
	static final int DATABASE_VERSION = 1;
	
	public static final String CURRENT_SONG_TABLE = "current_song";
	public static final String COLUMN_SONG_ID = "song_id";
	public static final String COLUMN_ALBUM_ID = "album_id";	
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_ALBUM = "album";
	public static final String COLUMN_ARTIST = "artist";
	public static final String COLUMN_PATH = "path";
	
	public static final String NOW_PLAYING_TABLE = "nowPlaying";
	public static final String COLUMN_LIST_ID = "list_id";

    public static final String REPEAT_SHUFFLE_TABLE = "repeat_shuffle";
    public static final String COLUMN_REPEAT = "isRepeat";
    public static final String COLUMN_SHUFFLE = "isShuffle";
	
	public static void init(Context context) {
		if (null == instance) {
			instance = new Database(context);
		}
        Database.context = context;
	}

	public static SQLiteDatabase getDatabase() {
		if (null == database) {
			database = instance.getWritableDatabase();
		}
		return database;
	}

	public static void deactivate() {
		if (null != database && database.isOpen()) {
			database.close();
		}
		database = null;
		instance = null;
	}

	public static long saveCurrentSong(Song song) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_SONG_ID, song.getID());
		cv.put(COLUMN_ALBUM_ID, song.getAlbumID());
		cv.put(COLUMN_TITLE, song.getTitle());
		cv.put(COLUMN_ALBUM, song.getAlbum());
		cv.put(COLUMN_ARTIST, song.getArtist());
		cv.put(COLUMN_PATH, song.getPath());
		
		return getDatabase().insert(CURRENT_SONG_TABLE, null, cv);
	}
	
	public static long saveNowPlayingSong(Song song){
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_SONG_ID, song.getID());
		cv.put(COLUMN_ALBUM_ID, song.getAlbumID());
		cv.put(COLUMN_TITLE, song.getTitle());
		cv.put(COLUMN_ALBUM, song.getAlbum());
		cv.put(COLUMN_ARTIST, song.getArtist());
		cv.put(COLUMN_PATH, song.getPath());
		
		return getDatabase().insert(NOW_PLAYING_TABLE, null, cv);
	}
	
	public static int deleteSong(Song song, String table){
		return getDatabase().delete(table, COLUMN_SONG_ID + "=" + song.getID(), null);
	}
	
	public static int deleteAll(String table){
		return getDatabase().delete(table, "1", null);
	}
	
	public static Song getCurrentSong() {
		String[] columns = new String[] { 
			COLUMN_SONG_ID,
			COLUMN_ALBUM_ID,
			COLUMN_TITLE,
			COLUMN_ALBUM,
			COLUMN_ARTIST,
			COLUMN_PATH
		};
		Cursor c = getDatabase().query(CURRENT_SONG_TABLE, columns, null,
				null, null, null, null);
		Song song = null;
		
		if(c.moveToFirst()){
            File file = new File(Uri.parse(c.getString(5)).getPath());
            song =  new Song(c.getLong(0), c.getLong(1), c.getString(2),
					c.getString(3), c.getString(4), c.getString(5));
            if (!file.exists()){
                deleteSong(song, CURRENT_SONG_TABLE);
                song = null;
            }
        }
		c.close();
		return song;
	}

    public static long saveRepeatShuffle(int isRepeat, int isShuffle) {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_REPEAT, isRepeat);
        cv.put(COLUMN_SHUFFLE, isShuffle);

        return getDatabase().insert(REPEAT_SHUFFLE_TABLE, null, cv);
    }

    public static int isRepeat(){
        String[] columns = new String[] {
                COLUMN_REPEAT
        };
        Cursor c = getDatabase().query(REPEAT_SHUFFLE_TABLE, columns, null,
                null, null, null, null);
        int isRepeat = 0;

        if(c.moveToFirst())
            isRepeat = c.getInt(0);
        c.close();

        return isRepeat;
    }

    public static int isShuffle(){
        String[] columns = new String[] {
                COLUMN_SHUFFLE
        };
        Cursor c = getDatabase().query(REPEAT_SHUFFLE_TABLE, columns, null,
                null, null, null, null);
        int isShuffle = 0;

        if(c.moveToFirst())
            isShuffle = c.getInt(0);
        c.close();

        return isShuffle;
    }
	
	public static Cursor getCursor() {
		String[] columns = new String[] { 
				COLUMN_LIST_ID,
				COLUMN_SONG_ID,
				COLUMN_ALBUM_ID,
				COLUMN_TITLE,
				COLUMN_ALBUM,
				COLUMN_ARTIST,
				COLUMN_PATH
				};
		return getDatabase().query(NOW_PLAYING_TABLE, columns,
				null, null, null, null, null);
	}

	Database(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS " + CURRENT_SONG_TABLE + " ( " 
				+ COLUMN_SONG_ID + " INTEGER primary key, "
				+ COLUMN_ALBUM_ID + " INTEGER NOT NULL, " 
				+ COLUMN_TITLE + " TEXT NOT NULL, " 
				+ COLUMN_ALBUM + " TEXT NOT NULL, " 
				+ COLUMN_ARTIST + " TEXT NOT NULL, "
				+ COLUMN_PATH + " TEXT NOT NULL)");
		db.execSQL("CREATE TABLE IF NOT EXISTS " + NOW_PLAYING_TABLE + " ( " 
				+ COLUMN_LIST_ID + " INTEGER primary key autoincrement, "
				+ COLUMN_SONG_ID + " INTEGER NOT NULL, "
				+ COLUMN_ALBUM_ID + " INTEGER NOT NULL, " 
				+ COLUMN_TITLE + " TEXT NOT NULL, " 
				+ COLUMN_ALBUM + " TEXT NOT NULL, " 
				+ COLUMN_ARTIST + " TEXT NOT NULL, "
				+ COLUMN_PATH + " TEXT NOT NULL)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + REPEAT_SHUFFLE_TABLE + " ( "
                + COLUMN_LIST_ID + " INTEGER primary key autoincrement, "
                + COLUMN_REPEAT + " INTEGER NOT NULL, "
                + COLUMN_SHUFFLE + " INTEGER NOT NULL)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + CURRENT_SONG_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + NOW_PLAYING_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + REPEAT_SHUFFLE_TABLE);
		onCreate(db);
	}

	public static ArrayList<Song> getNowPlayingList() {
		ArrayList<Song> songList = new ArrayList<>();
		Cursor cursor = Database.getCursor();
		Song song;
		if (cursor.moveToFirst()) {

			do {
				song =  new Song(cursor.getLong(1), cursor.getLong(2), cursor.getString(3),
                        cursor.getString(4), cursor.getString(5), cursor.getString(6));

                File file = new File(Uri.parse(cursor.getString(6)).getPath());
                if (!file.exists()){
                    deleteSong(song, NOW_PLAYING_TABLE);
                } else{
                    songList.add(song);
                }
            } while (cursor.moveToNext());
		}
		cursor.close();
		return songList;
	}
}