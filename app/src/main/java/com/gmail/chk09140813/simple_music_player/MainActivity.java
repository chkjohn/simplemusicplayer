package com.gmail.chk09140813.simple_music_player;

import android.app.ListActivity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Project: SimpleMusicPlayer
 * Arthur:  Chan Ho Kwan (John)
 */

public class MainActivity extends ListActivity implements
        View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    // Keys to identify media player actions
    private static final int PREV = 0;
    private static final int PLAY = 1;
    private static final int NEXT = 2;
    private static final int REPEAT = 3;
    private static final int SHUFFLE = 4;
    private static final int CANCEL = 5;
    private static final int COMPLETE = 6;

    // View objects of the fragment layout
    private ImageView coverArt;
    private MarqueeText songTitle;
    private TextView currentPos;    // TextView to show the current position of media player
    private TextView duration;      // TextView to show the duration of current song

    private ImageButton btnPrev;
    private ImageButton btnPlay;
    private ImageButton btnNext;

    private Drawable playIcon;
    private Drawable pauseIcon;

    private MenuItem btnRepeat;
    private MenuItem btnShuffle;
    private SeekBar seekBar;

    private View undoBar;
    private TextView undo_message;

    private ListView songList;      // ListView of all song
    private Integer currentSong;	// position of current playing song
    private SongListAdapter songListAdapter;
    private Bitmap defaultCover;
    private ArrayList<Song> songToDelete;   // list of all songs to be deleted
    private ArrayList<Integer> deletePos;   // list of original pos of all
                                            // to-be-deleted songs in playlist

    private boolean isPlaying = false;

    private Handler mHandler;
    private Runnable mRunnable;
    private Intent musicPlayerIntent;
    private ServiceConnection mServiceConn;
    private MusicPlayerInterface musicPlayerService;

    /**
     * Handler of incoming messages from service.
     */
    private class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Log.d("handleMessage", Integer.toString(msg.what));
            switch(msg.what){
                case PLAY:
                    Play();
                    break;
                case REPEAT:
                    Repeat(btnRepeat);
                    break;
                case SHUFFLE:
                    Shuffle(btnShuffle);
                    break;
                case CANCEL:
                    Cancel();
                    break;
                case COMPLETE:
                    mHandler.removeCallbacks(mRunnable);
                    btnPlay.setImageResource(android.R.drawable.ic_media_play);
                    seekBar.setProgress(0);
                    currentPos.setText(DateUtils.formatElapsedTime(0));
                    break;
                case NEXT:
                    Next();
                case PREV:
                    setPlaySongUI();
                    songList.setItemChecked(currentSong, true);
                    songList.setSelection(currentSong);
            }
            super.handleMessage(msg);
        }
    }

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    private final Messenger mMessenger = new Messenger(new IncomingHandler());

    public static enum repeatState{
        OFF,
        ON,
        ONE
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        undoBar = findViewById(R.id.undo_bar);
        undo_message = (TextView) findViewById(R.id.undo_message);
        defaultCover = BitmapFactory.decodeResource(
                getResources(), R.drawable.cover);

        songToDelete = new ArrayList<>();
        deletePos = new ArrayList<>();

        /**
         *  create and set adapter of playlist
         */
        songListAdapter = new SongListAdapter(this);
        setListAdapter(songListAdapter);
        songList = (ListView) findViewById(android.R.id.list);
        if (savedInstanceState != null)
            songList.setSelection(savedInstanceState.getInt("currentSong"));
        songList.setOnTouchListener(new SwipeDismissListViewTouchListener(
                songList, new SwipeDismissListViewTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(int position) {
                return true;
            }

            @Override
            public void onDismiss(ListView listView, int[] reverseSortedPositions) {
                for (int position : reverseSortedPositions){
                    Song deleteSong = (Song) songListAdapter.getItem(position);
                    songToDelete.add(deleteSong);
                    deletePos.add(position);
                    undo_message.setText("Song \"" + deleteSong.getTitle() + "\" deleted");
                    songListAdapter.remove(position);
                    try {
                        musicPlayerService.deleteSong(position);
                        Intent intent = new Intent(
                                getApplicationContext(), MusicPlayerService.class);
                        startService(intent);
                    } catch (RemoteException e) {
                        //e.printStackTrace();
                    }
                    songList.invalidateViews();
                    setPlaySongUI();
                    songList.setItemChecked(currentSong, true);
                }
                undoBar.setVisibility(View.VISIBLE);
                undoBar.setAlpha(1);
                undoBar.animate().alpha(0f).setDuration(5000)
                        .withEndAction(new Runnable() {

                            @Override
                            public void run() {
                                undoBar.setVisibility(View.GONE);
                            }
                        });
            }
        }));
        Button undo = (Button) findViewById(R.id.undo_button);
        undo.setOnClickListener(this);

        /**
         *  set up service connection for binding the music player service
         */
        musicPlayerIntent = new Intent(this, MusicPlayerService.class);
        musicPlayerIntent.putExtras(getIntent());
        mServiceConn = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                // We've bound to MusicPlayerService, cast the IBinder and
                // get MusicPlayerService instance
                musicPlayerService = MusicPlayerInterface.Stub.asInterface(iBinder);
                try {
                    musicPlayerService.setMessenger(mMessenger);
                } catch (RemoteException e) {
                    //e.printStackTrace();
                }

                try{
                    songTitle = (MarqueeText) findViewById(R.id.song_title);
                    songTitle.setEnabled(true);

                    currentPos = (TextView) findViewById(R.id.currentPos);

                    duration = (TextView) findViewById(R.id.duration);

                    btnPrev = (ImageButton) findViewById(R.id.btnPrev);
                    btnPrev.setOnClickListener(MainActivity.this);

                    btnPlay = (ImageButton) findViewById(R.id.btnPlay);
                    btnPlay.setOnClickListener(MainActivity.this);

                    btnNext = (ImageButton) findViewById(R.id.btnNext);
                    btnNext.setOnClickListener(MainActivity.this);

                    seekBar = (SeekBar) findViewById(R.id.seekBar1);
                    seekBar.setOnSeekBarChangeListener(MainActivity.this);

                    coverArt = (ImageView) findViewById(R.id.cover_art);

                    setPlaySongUI();
                    songList.setItemChecked(currentSong, true);
                    songList.setSelection(currentSong);

                    Repeat(btnRepeat);
                    Shuffle(btnShuffle);
                } catch (Exception e){
                    //e.printStackTrace();
                }

            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                musicPlayerService = null;
                //finish();
            }
        };

        /**
         *  set up handler for the seek bar
         */
        mHandler = new Handler();
        mRunnable = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (musicPlayerService != null ){
                    int mCurrentPos;

                    try {
                        mCurrentPos = musicPlayerService.getCurrentPosition();
                    } catch (Exception e){
                        mCurrentPos = 0;
                    }

                    currentPos.setText(DateUtils.formatElapsedTime(mCurrentPos / 1000));
                    seekBar.setProgress(mCurrentPos);
                }
                mHandler.post(this);
            }
        };
        mRunnable.run();
        mHandler.removeCallbacks(mRunnable);

        startService(musicPlayerIntent);

        (new SimpleEULA(this)).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        bindService(musicPlayerIntent, mServiceConn, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        mHandler.removeCallbacks(mRunnable);
        for (Song song : songToDelete){
            File songFile = new File(song.getPath());
            songFile.delete();
        }
        unbindService(mServiceConn);
        if (!isPlaying)
            stopService(musicPlayerIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //trimCache(this);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("currentSong", currentSong);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle state) {
        super.onRestoreInstanceState(state);

        if (songList != null){
            songList.setSelection(state.getInt("currentSong"));
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        //Log.d("onClick", "button clicked");
        switch(v.getId()){
            case R.id.btnPlay:
                if (songList.getCount() > 0) {
                    try {
                        musicPlayerService.Play();
                    } catch (RemoteException e) {
                        //e.printStackTrace();
                    }
                    Play();
                    Intent intent = new Intent(this, MusicPlayerService.class);
                    startService(intent);
                }
                break;
            case R.id.btnPrev:
                if (songList.getCount() > 0) {
                    Prev();
                }
                break;
            case R.id.btnNext:
                if (songList.getCount() > 0) {
                    Next();
                }
                break;
            case R.id.undo_button:
                undoBar.setVisibility(View.GONE);
                int i = songToDelete.size() - 1;
                Song undoSong = songToDelete.get(i);
                int undoPos = deletePos.get(i);
                songListAdapter.add(undoPos, undoSong);
                try {
                    musicPlayerService.undoDeletion(undoPos, undoSong);
                } catch (RemoteException e) {
                    //e.printStackTrace();
                }
                deletePos.remove(i);
                songToDelete.remove(i);
                songList.invalidateViews();
                setPlaySongUI();
                songList.setItemChecked(currentSong, true);

                Intent intent = new Intent(this, MusicPlayerService.class);
                startService(intent);
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        //Log.d("onListItemClick", "item clicked");

        try {
            musicPlayerService.setCurrentSong(position);
        } catch (RemoteException e) {
            //e.printStackTrace();
        }
        try {
            musicPlayerService.playSong(
                    (Song) l.getItemAtPosition(position));
            Intent intent = new Intent(this, MusicPlayerService.class);
            startService(intent);
        } catch (RemoteException e) {
            //e.printStackTrace();
        }

        setPlaySongUI();
        l.setItemChecked(currentSong, true);
        l.setSelection(currentSong);

        mHandler.removeCallbacks(mRunnable);
        mHandler.post(mRunnable);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        btnRepeat = menu.findItem(R.id.btnRepeat);
        btnShuffle = menu.findItem(R.id.btnShuffle);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        try {
            Repeat(btnRepeat);
            Shuffle(btnShuffle);
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        //Log.d("onClick", "button clicked");
        switch(item.getItemId()){
            case R.id.btnShuffle:
                try {
                    musicPlayerService.Shuffle();
                } catch (RemoteException e) {
                    //e.printStackTrace();
                }
                Shuffle(item);
                break;
            case R.id.btnRepeat:
                try {
                    musicPlayerService.Repeat();
                } catch (RemoteException e) {
                    //e.printStackTrace();
                }
                Repeat(item);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (level >= TRIM_MEMORY_RUNNING_LOW)
            System.gc();
    }

    /**
     * change the icon of 'play' button
     */
    private void Play() {
        if (playIcon == null || pauseIcon == null){
            playIcon = getResources().getDrawable(R.drawable.ic_action_play);
            pauseIcon = getResources().getDrawable(R.drawable.ic_action_pause);
        }
        try {
            if (musicPlayerService.isPlaying()){
                mHandler.post(mRunnable);
                btnPlay.setImageDrawable(pauseIcon);
                isPlaying = true;
            } else{
                mHandler.removeCallbacks(mRunnable);
                btnPlay.setImageDrawable(playIcon);
                isPlaying = false;
            }
        } catch (RemoteException e) {
            //e.printStackTrace();
        }
    }

    /**
     * send request to musicPlayerService for playing the previous song
     */
    private void Prev() {
        try {
            musicPlayerService.Prev(currentSong);
        } catch (RemoteException e) {
            //e.printStackTrace();
        }

        setPlaySongUI();
        songList.setItemChecked(currentSong, true);
        songList.setSelection(currentSong);

        mHandler.removeCallbacks(mRunnable);
        mHandler.post(mRunnable);

        Intent intent = new Intent(this, MusicPlayerService.class);
        startService(intent);
    }

    /**
     * send request to musicPlayerService for playing the next song
     */
    private void Next() {
        try {
            musicPlayerService.Next(currentSong);
        } catch (RemoteException e) {
            //e.printStackTrace();
        }

        setPlaySongUI();
        songList.setItemChecked(currentSong, true);
        songList.setSelection(currentSong);

        mHandler.removeCallbacks(mRunnable);
        mHandler.post(mRunnable);

        Intent intent = new Intent(this, MusicPlayerService.class);
        startService(intent);
    }

    /**
     * change the icon of 'shuffle' button
     * @param item 'shuffle' action button
     */
    private void Shuffle(MenuItem item) {
        try {
            if(musicPlayerService.isShuffle()){
                item.setIcon(R.drawable.ic_action_shuffle_on);
            } else{
                item.setIcon(R.drawable.ic_action_shuffle_off);
            }
        } catch (RemoteException e) {
            //e.printStackTrace();
        }
    }

    /**
     * change the icon of 'repeat' button
     * @param item 'repeat' action button
     */
    private void Repeat(MenuItem item) {
        try {
            switch(musicPlayerService.getRepeatState()){
                case 0:	// repeat OFF
                    item.setIcon(R.drawable.ic_action_repeat_off);
                    break;
                case 1:	// repeat ON
                    item.setIcon(R.drawable.ic_action_repeat_on);
                    break;
                case 2:	// repeat one song only
                    item.setIcon(R.drawable.ic_action_repeat_one);
                    break;
            }
        } catch (RemoteException e) {
            //e.printStackTrace();
        }
    }

    /**
     * close the activity
     */
    private void Cancel(){
        finish();
    }

    /**
     * config the UI of the music player view
     */
    private void setPlaySongUI(){
        try {
            currentSong = musicPlayerService.getCurrentSong();
        } catch (RemoteException e) {
            //e.printStackTrace();
        }

        if (songListAdapter.getCount() > 0) {
            Song song = (Song) songList.getItemAtPosition(currentSong);

            Uri sArtworkUri = Uri.parse("content://media/external/audio/albumart");
            Uri uri = ContentUris.withAppendedId(sArtworkUri, song.getAlbumID());
            Picasso.with(this)
                    .load(uri)
                    .placeholder(R.drawable.cover)
                    .error(R.drawable.cover)
                    .resizeDimen(R.dimen.activity_image_size, R.dimen.activity_image_size)
                    //.centerInside()
                    .centerCrop()
                    .tag(this)
                    .into(coverArt);
            songTitle.setText(song.getTitle());
            try {
                duration.setText(DateUtils.formatElapsedTime(
                        musicPlayerService.getDuration() / 1000));
                seekBar.setMax(musicPlayerService.getDuration());
            } catch (RemoteException e) {
                //e.printStackTrace();
            }
            int mCurrentPos;
            try {
                mCurrentPos = musicPlayerService.getCurrentPosition();
            } catch (Exception e) {
                mCurrentPos = 0;
            }
            currentPos.setText(DateUtils.formatElapsedTime(mCurrentPos / 1000));
            Play();
        } else {
            mHandler.removeCallbacks(mRunnable);
            coverArt.setImageBitmap(defaultCover);
            songTitle.setText(R.string.no_song);
            duration.setText(null);
            seekBar.setMax(0);
            currentPos.setText(null);
            Play();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        // TODO Auto-generated method stub
        if (musicPlayerService != null && fromUser){
            try {
                musicPlayerService.seekTo(progress);
            } catch (RemoteException e) {
                //e.printStackTrace();
            }
            currentPos.setText(DateUtils.formatElapsedTime(progress / 1000));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub
        try {
            if (musicPlayerService.isPlaying()){
                musicPlayerService.pause();
                mHandler.removeCallbacks(mRunnable);
            }
        } catch (RemoteException e) {
            //e.printStackTrace();
        }
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // TODO Auto-generated method stub
        try {
            if (musicPlayerService.isPlaying()){
                musicPlayerService.start();
                mHandler.post(mRunnable);
            }
        } catch (RemoteException e) {
            //e.printStackTrace();
        }
    }
}
