/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.chk09140813.simple_music_player;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;

public class SimpleEULA {
	
	private Activity mActivity;
	private static final String TAG = "getPackageInfo_class";
	private static final String MSG = "PackageManager_NameNotFoundException";
	
	public SimpleEULA(Activity context)		{
		mActivity = context; 
	}
	
	private PackageInfo getPackageInfo()	{
		
		PackageInfo aPackageInfo = null; 
		
		try{
			aPackageInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), PackageManager.GET_ACTIVITIES); 
		} catch (PackageManager.NameNotFoundException e)	{
			Log.e(TAG, MSG); 
		}

		return aPackageInfo; 
	}
	
	public void show() {
		
		String EULA_PREFIX = "eula_"; 		
		PackageInfo versionInfo = getPackageInfo(); 
		
		// the eulaKey can be modified in AndroidManifest.xml
		final String eulaKey = EULA_PREFIX + versionInfo.versionCode;
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity); 
		boolean hasBeenShown = prefs.getBoolean(eulaKey, false); 
		
		if(!hasBeenShown) {
			
			// show the EULA
			String eula_title = mActivity.getString(R.string.app_name) + " v" + versionInfo.versionName; 
			
			// includes the updates as well so that users know what changes
			// String eula_message = mActivity.getString(R.string.app_updates) + "\n\n" + mActivity.getString(R.string.app_eula); 
			String eula_message = mActivity.getString(R.string.app_eula);
			
			AlertDialog.Builder aBuilder = new AlertDialog.Builder(mActivity)
				.setTitle(eula_title)
				.setMessage(eula_message)
                .setCancelable(false)
				//.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
				.setPositiveButton("I Accept", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						// mark this version as READ
						SharedPreferences.Editor editor = prefs.edit();
						
						editor.putBoolean(eulaKey, true); 
						// editor.commit();
                        editor.apply();
						dialog.dismiss(); 	
						
						//mActivity.setContentView(R.layout.activity_main);
					}
				})
				//.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
				.setNegativeButton("I Decline", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {

						// close the activity as they have declined the EULA
						mActivity.finish();
					}
				});
			
			aBuilder.create().show();
		} /*else {
			mActivity.setContentView(R.layout.activity_main);
		}*/
	}
}
