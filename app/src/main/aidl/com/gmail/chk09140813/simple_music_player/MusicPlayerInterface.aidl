/*
 * Copyright 2013 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// IMyAidlInterface.aidl
package com.gmail.chk09140813.simple_music_player;

// Declare any non-default types here with import statements
import com.gmail.chk09140813.simple_music_player.Song;
import android.os.Messenger;

interface MusicPlayerInterface {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void basicTypes(int anInt, long aLong, boolean aBoolean, float aFloat,
            double aDouble, String aString);

    int getPid();

    /**
     * Pass a Messenger from client to service so that the service can send
     * feedback to the client
     * @param replyTo Messenger from client
     */
    void setMessenger(in Messenger replyTo);

    /**
     * Return the duration of current song
     * @return the duration in milliseconds, if no duration is available
     * (for example, if streaming live content), -1 is returned.
     */
    int getDuration();

    /**
     * Return the current position of music player
     * @return the current position in milliseconds
     */
    int getCurrentPosition();

    /**
     * Seek the music player to specific time position
     * @param progress the offset in milliseconds from the start to seek to
     */
    void seekTo(int progress);

    /**
     * start the music player
     */
    void start();

    /**
     * pause the music player
     */
    void pause();

    /**
     * check if the music player is playing
     * @return true if playing, false otherwise
     */
    boolean isPlaying();

    /**
     * Return the current state of 'repeat' function (OFF, ON, ONE)
     * @return An integer representing the current state of 'repeat' function
     */
    int getRepeatState();

    /**
     * check if the 'shuffle' function is ON
     * @return true for ON, false for OFF
     */
    boolean isShuffle();

    /**
     * get the position of current song
     * @return position of the song in the song list
     */
    int getCurrentSong();

    /**
     * get the position of last song
     * @return position of the song in the song list
     */
    int getLastSong();

    /**
     * select the song at position 'current' as current song
     * @param current position of the song
     */
    void setCurrentSong(int current);

    /**
     * delete the song at the index 'position' of song list
     * @param position position of the song to be deleted
     */
    void deleteSong(int position);

    /**
     * undo the previous deletion
     * @param position original position of the deleted song in
     *                 the song list
     * @param song the deleted song
     */
    void undoDeletion(int position, in Song tmp);

    /**
     * toggle the 'play' function
     */
    void Play();

    /**
     * select and play the previous song
     * @param currentSong position of current song
     */
    void Prev(int current);

    /**
     * select and play the next song
     * @param currentSong position of current song
     */
    void Next(int current);

    /**
     * toogle the 'shuffle' function
     */
    void Shuffle();

    /**
     * toggle the 'repeat' function
     */
    void Repeat();

    /**
     * reset, prepare and start the media player
     * @param song the song that will be played
     */
    void playSong(in Song song);
}
